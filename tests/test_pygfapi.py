#!env/bin/python
"""
Test cases for pygfapi.
"""

import unittest
from pygfapi import Client


class ClientTestCase(unittest.TestCase):
    def setUp(self):
        self.gfclient = Client(
            "http://mydomain.com/gravityformsapi/",
            "1234",
            "abcd"
        )

    def test_init(self):
        self.assertEqual(
            self.gfclient.api_url,
            "http://mydomain.com/gravityformsapi/"
        )
        self.assertEqual(self.gfclient.public_key, "1234")
        self.assertEqual(self.gfclient.private_key, "abcd")
